package es.uam.fernando.damas;
import java.util.Scanner;

import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

public class JugadorHumano implements Jugador {
	private String nombre;
	private static int numHumanos = 0;

	/** Constructor por defecto */
	public JugadorHumano() {
		this("Humano " + (++numHumanos));
	}

	/** Creates a new instance of JugadorAleatorio */
	public JugadorHumano(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public void onCambioEnPartida(Evento evento) {
		switch (evento.getTipo()) {

		case Evento.EVENTO_CAMBIO:

			((TableroReversi) evento.getPartida().getTablero()).evaluarEstado();
			
			if((evento.getPartida().getTablero().getEstado() == Tablero.FINALIZADA)){
				System.out.println("La partida ha terminado");
				int scorePlayer1 = ((TableroReversi) evento.getPartida().getTablero()).getScorePlayer1();
				int scorePlayer2 = ((TableroReversi) evento.getPartida().getTablero()).getScorePlayer2();
				if(scorePlayer1 > scorePlayer2){
					System.out.println("Has Perdido");	
					System.out.println("Tus Fichas " + scorePlayer2 + "Fichas contrincante " + scorePlayer1) ;	
				}else{
					System.out.println("Has ganado");
					System.out.println("Tus Fichas " + scorePlayer2 + "Fichas contrincante " + scorePlayer1) ;
				}
			}else{
				System.out.println(evento.getDescripcion());
				evento.getPartida().getTablero().toString();
			}			
			break;

		case Evento.EVENTO_CONFIRMA:

			try {
				evento.getPartida().confirmaAccion(this, evento.getCausa(), true);
			} catch (Exception e) {

			}
			break;

		case Evento.EVENTO_TURNO:
			Tablero t = evento.getPartida().getTablero();
			boolean valido = true;
			int r = -1;
			int f = -1;
			Scanner columna;
			Scanner fila;
			if ((evento.getPartida().getTablero().getEstado() != Tablero.TABLAS)
					&& (evento.getPartida().getTablero().getEstado() != Tablero.FINALIZADA)) {
				MovimientoReversi m = null;
				while (valido) {
					System.out.println(evento.getDescripcion());
					System.out.println("Seleccione la columna donde jugar:");
					columna = new Scanner(System.in);
					r = columna.nextInt();
					System.out.println("Seleccione la fila donde jugar:");
					fila = new Scanner(System.in);
					f = fila.nextInt();
					m = new MovimientoReversi(r, f);
					if ((r > 7 || r < 0 ) && (f > 7 || f < 0)) {
						System.out.println("Debe introducir una columna valida y una fila, valor entre 0 y 7");
					} else {
						if (!evento.getPartida().getTablero().esValido(new MovimientoReversi(r, f))){
							System.out.println("Movimiento invalido, elija otro");
							System.out.println("Movimientos permitidos: ");
							for (Movimiento item : t.movimientosValidos()) {
								System.out.println("Movimiento: ");
								System.out.println(item);
							}
							valido = true;
						} else {
							valido = false;
						}
					}
				}

				try {
					int index = t.movimientosValidos().indexOf((Movimiento)m);
					evento.getPartida().realizaAccion(new AccionMover(this, t.movimientosValidos().get(index)));					
				} catch (ExcepcionJuego e) {
					e.getStackTrace();
				}
			} else {
				try {
					evento.getPartida().realizaAccion(new AccionMover(this, t.movimientosValidos().get(0)));
				} catch (ExcepcionJuego e) {
					e.printStackTrace();
				}
			}
			break;

		}
	}

	@Override
	public String getNombre() {
		return this.nombre;
	}

	@Override
	public boolean puedeJugar(Tablero tablero) {
		return true;
	}

}
