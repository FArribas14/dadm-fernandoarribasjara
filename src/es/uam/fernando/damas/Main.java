package es.uam.fernando.damas;
import java.util.ArrayList;

import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.JugadorAleatorio;
import es.uam.eps.multij.Partida;

public class Main {

	public static void main(String[] args) throws ExcepcionJuego {
		
		Jugador jugador1 = new JugadorAleatorio("Maquina");
		Jugador jugador2 = new JugadorHumano("Humano");		
		
		
		//Jugador jugador3 = new JugadorHumano("Humano");
		
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
		jugadores.add(jugador1);
		jugadores.add(jugador2);
		
		Partida partida = new Partida(new TableroReversi(), jugadores);
		//partida.addObservador(jugador3);
		partida.comenzar();
	}

}
