package es.uam.fernando.damas;

import es.uam.eps.multij.Movimiento;

public class MovimientoReversi extends Movimiento {

	private int col;
	private int fil;

	public MovimientoReversi(int c, int f) {
		col = c;
		fil = f;
	}

	@Override
	public String toString() {
		return "Columna " + col + "Fila " + fil;
	}

	@Override
	public boolean equals(Object o) {
        if (o == null) {
                return false;
        }
        MovimientoReversi m = (MovimientoReversi)o;
        if (this.col == m.col && this.fil == m.fil) {
            return true;
        }
        return false;
    }
	

	public int getValorCol() {
		return this.col;
	}

	public int getValorFil() {
		return this.fil;
	}

	public void setValorCol(int c) {
		this.col = c;
	}

	public void setValorFil(int f) {
		this.fil = f;
	}
}
