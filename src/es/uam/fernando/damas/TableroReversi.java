package es.uam.fernando.damas;
import java.util.ArrayList;

import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

public class TableroReversi extends Tablero {

	/* Numero de columnas */
	public static final int COLUMNAS = 8;
	
	/* Numero de filas */
	public static final int FILAS = 8;

	/*Tablero*/
	int tab[][];
	int ultimoJugador;
	int ultimaFila;
	int ultimaColumna;
	int jugador;
	public int numFichasPlayer1;
	public int numFichasPlayer2;
	
	public TableroReversi() {
		
		tab = new int[COLUMNAS][FILAS];
		for(int i=0; i<COLUMNAS;i++){
			for(int j=0; j<FILAS;j++){
				if (( i== 3 && j == 3) || ( i== 4 && j == 4)){
					tab[i][j] = 1;
				}else if (( i== 4 && j == 3) || ( i== 3 && j == 4)){
					tab[i][j] = 2;
				}else{
					tab[i][j] = 0;
				}
				
			}
		}
		
		this.estado = EN_CURSO;
		this.numJugadas = 0;
		this.numJugadores = 2;
		this.turno =1;
		this.ultimoMovimiento = null;
		ultimoJugador=1;
		numFichasPlayer1 = 0;
		numFichasPlayer2 = 0;
	}
	

	@Override 
	public void mueve(Movimiento m) throws ExcepcionJuego {
		
		if(estado == Tablero.FINALIZADA || estado == Tablero.TABLAS){
			cambiaTurno();
			return;
		}
		try {
			if(esValido((MovimientoReversi) m)){
				
				int col = ((MovimientoReversi) m).getValorCol();
				int fil = ((MovimientoReversi) m).getValorFil();
				
				for(int i=0; i<COLUMNAS; i++){
					if(col == i){
						for(int j=FILAS-1; j>=0;j--){
							if(fil == j){
								if(tab[i][j]==0){
									situarFicha((MovimientoReversi) m);
									tab[col][fil] = turno + 1;
									ultimoJugador = turno + 1;
									ultimoMovimiento = m;
									ultimaFila = j;
									ultimaColumna = i;
									break;
								}
							}
						}
					}
				}
				
				if(estado != Tablero.FINALIZADA || estado != Tablero.TABLAS){
					if(turno == 1){
						numFichasPlayer2++;
					}else{
						numFichasPlayer1++;
					}
					this.cambiaTurno();
				}
			} else {
				throw new ExcepcionJuego("El movimiento no es valido");
			}
		} catch (ExcepcionJuego e){
			e.getStackTrace();
		}
	}

	public void evaluarEstado() {

		if(numFichasPlayer1 == 32 || numFichasPlayer2 == 4){
			estado =Tablero.FINALIZADA;
		}
		
		/*
		if(movimientosValidos().isEmpty() && estado != Tablero.FINALIZADA){
			estado = Tablero.TABLAS;
		}*/
	}
	
	public int getScorePlayer1() {
	      return numFichasPlayer1;
	}
	
	public int getScorePlayer2() {
	      return numFichasPlayer2;
	}
	
	@Override
	public boolean esValido(Movimiento m) {
		ArrayList<Movimiento> ret = this.movimientosValidos();
		MovimientoReversi m2 = (MovimientoReversi) m;
		if (ret.contains(m2))
				return true;
						
		
		return false;

	}

	@Override
	public ArrayList<Movimiento> movimientosValidos() {
		ArrayList<Movimiento> ret = new ArrayList<Movimiento>();

      if(ultimoJugador == 1){
    	  jugador = 2;
      }else{
    	  jugador = 1;
      }
		for (int i = 0; i < COLUMNAS; i++) {
			for (int j = 0; j < FILAS; j++) {
				Movimiento m = new MovimientoReversi(i, j);
				if (tab[i][j] == 0 && moviemientoValido((MovimientoReversi) m)) {					
					ret.add(m);
				}				
			}
		}

		return ret;
	}
	
	public boolean moviemientoValido (MovimientoReversi m) {
	    boolean isValid = false;
	    
	  if(comprobarMovimientoArriba(m) || comprobarMovimientoAbajo(m) || 
	     comprobarMovimientoDerecha(m) ||comprobarMovimientoIzquierda(m) ||
	     comprobarMovimientoArribaDerecha(m) || comprobarMovimientoArribaIzquierda(m) ||
	     comprobarMovimientoAbajoDerecha(m) || comprobarMovimientoAbajoIzquierda(m)){
		 isValid = true;
	  }      
	    return isValid;
	  }
	
	 private boolean[] comprobarMovimientoCorrecto(MovimientoReversi movimiento) {
	      boolean[] movimientosValidos = new boolean[8];
	      movimientosValidos[0] = comprobarMovimientoArriba(movimiento);
	      movimientosValidos[1] = comprobarMovimientoAbajo(movimiento);
	      movimientosValidos[2] = comprobarMovimientoDerecha(movimiento);
	      movimientosValidos[3] = comprobarMovimientoIzquierda(movimiento);
	      movimientosValidos[4] = comprobarMovimientoArribaDerecha(movimiento);
	      movimientosValidos[5] = comprobarMovimientoArribaIzquierda(movimiento);
	      movimientosValidos[6] = comprobarMovimientoAbajoDerecha(movimiento);
	      movimientosValidos[7] = comprobarMovimientoAbajoIzquierda(movimiento);
	      return movimientosValidos;
	   }
	
	 public boolean comprobarMovimientoArriba(MovimientoReversi movimiento) {
	      int fila = movimiento.getValorFil();
	      int columna = movimiento.getValorCol();
	      
	      if (tab[columna][fila] != 0 || (fila <= 1)) {
	         return false;
	      }
	      
	      if (tab[columna][fila - 1] != ultimoJugador) {
	         return false;
	      }

	      int i = fila - 1;
	      while ((i > 0) && (tab[columna][i] == ultimoJugador)) {
	         i--;
	      }
	      
	      if (tab[columna][i] == jugador) {
	         return true;
	      }
	      else {
	         return false;
	      }
	   }

	 public boolean comprobarMovimientoAbajo(MovimientoReversi movimiento) {
	      int fila = movimiento.getValorFil();
	      int columna = movimiento.getValorCol();
	      
	      if (tab[columna][fila] != 0 || (fila >= 6)) {
	         return false;
	      }
	      if (tab[columna][fila + 1] != ultimoJugador) {
	         return false;
	      }
	      int i = fila + 1;
	      while ((i < FILAS - 1) && (tab[columna][i] == ultimoJugador)) {
	         i++;
	      }
	      if (tab[i][columna] == jugador) {
	         return true;
	      }
	      else {
	         return false;
	      }
	   }
	 
	
	   public boolean comprobarMovimientoIzquierda(MovimientoReversi movimiento) {
	      int fila = movimiento.getValorFil();
	      int columna = movimiento.getValorCol();
	      if (tab[columna][fila] != 0 || (columna <= 1)) {
	         return false;
	      }
	      if (tab[columna -1 ][fila] != ultimoJugador) {
	         return false;
	      }
	      int i = columna - 1;
	      while ((i > 0) && (tab[i][fila] == ultimoJugador)) {
	         i--;
	      }
	      if (tab[i][fila] == jugador) {
	         return true;
	      }
	      else {
	         return false;
	      }
	   }

	   
	   public boolean comprobarMovimientoDerecha(MovimientoReversi movimiento) {
	      int fila = movimiento.getValorFil();
	      int columna = movimiento.getValorCol();
	      if (tab[columna][fila] !=0  || (columna >= 6)) {
	         return false;
	      }
	      if (tab[columna + 1][fila] != ultimoJugador) {
	         return false;
	      }
	      int i = columna + 1;
	      while ((i < COLUMNAS - 1) && (tab[i][fila] == ultimoJugador)) {
	         i++;
	      }
	      if (tab[i][fila] == jugador) {
	         return true;
	      }
	      else {
	         return false;
	      }
	   }

	   
	   public boolean comprobarMovimientoArribaDerecha(MovimientoReversi movimiento) {
	      int fila = movimiento.getValorFil();
	      int columna = movimiento.getValorCol();
	      if (tab[columna][fila] != 0 || (columna >= 6) || (fila <= 1)) {
	         return false;
	      }
	      if (tab[columna + 1][fila - 1] != ultimoJugador) {
	         return false;
	      }
	      int i = fila - 1;
	      int j = columna + 1;
	      while ((i > 0) && (j < COLUMNAS - 1) && (tab[j][i] == ultimoJugador)) {
	         i--;
	         j++;
	      }
	      if (tab[j][i] == jugador) {
	         return true;
	      }
	      else {
	         return false;
	      }
	   }

	   
	   public boolean comprobarMovimientoArribaIzquierda(MovimientoReversi movimiento) {
	      int fila = movimiento.getValorFil();
	      int columna = movimiento.getValorCol();
	      if (tab[columna][fila] != 0 || (columna <= 1) || (fila <= 1)) {
	         return false;
	      }
	      if (tab[columna - 1][fila - 1] != ultimoJugador) {
	         return false;
	      }
	      int i = fila - 1;
	      int j = columna - 1;
	      while ((i > 0) && (j > 0) && (tab[j][i] == ultimoJugador)) {
	         i--;
	         j--;
	      }
	      if (tab[j][i] == jugador) {
	         return true;
	      }
	      else {
	         return false;
	      }
	   }

	   
	   public boolean comprobarMovimientoAbajoDerecha(MovimientoReversi movimiento) {
	      int fila = movimiento.getValorFil();
	      int columna = movimiento.getValorCol();
	      if (tab[columna][fila] != 0 || (columna >= 6) || (fila >= 6)) {
	         return false;
	      }
	      if (tab[columna + 1][fila + 1] != ultimoJugador) {
	         return false;
	      }
	      int i = fila + 1;
	      int j = columna + 1;
	      while ((i < FILAS - 1) && (j < COLUMNAS - 1) && (tab[j][i] == ultimoJugador)) {
	         i++;
	         j++;
	      }
	      if (tab[j][i] == jugador) {
	         return true;
	      }
	      return false;
	   }

	   
	   public boolean comprobarMovimientoAbajoIzquierda(MovimientoReversi movimiento) {
	      int fila = movimiento.getValorFil();
	      int columna = movimiento.getValorCol();
	      if (tab[columna][fila] != 0 || (columna <= 1) || (fila >= 6)) {
	         return false;
	      }
	      if (tab[columna - 1][fila + 1] != ultimoJugador) {
	         return false;
	      }
	      int i = fila + 1;
	      int j = columna - 1;
	      while ((i < FILAS - 1) && (j > 0) && (tab[j][i] == ultimoJugador)) {
	         i++;
	         j--;
	      }
	      if (tab[j][i] == jugador) {
	         return true;
	      }
	      else {
	         return false;
	      }
	   }

	   
	   
	   private void convertirFichasArriba(MovimientoReversi movimiento) {
		  int fila = movimiento.getValorFil();
		  int columna = movimiento.getValorCol();
	      int i = fila - 1;
	      while (tab[columna][i] != jugador) {
	    	  tab[columna][i] = jugador;
	         i--;
	      }
	   }

	   
	   private void convertirFichasAbajo(MovimientoReversi movimiento) {
		  int fila = movimiento.getValorFil();
		  int columna = movimiento.getValorCol();
	      int i = fila + 1;
	      while (tab[columna][i] != jugador) {
	    	  tab[columna][i] = jugador;
	         i++;
	      }
	   }

	   private void convertirFichasDerecha(MovimientoReversi movimiento) {
		  int fila = movimiento.getValorFil();
		  int columna = movimiento.getValorCol();
	      int j = columna + 1;
	      while (tab[j][fila] != jugador) {
	    	  tab[j][fila] = jugador;
	         j++;
	      }
	   }

	   
	   private void convertirFichasIzquierda(MovimientoReversi movimiento) {
		  int fila = movimiento.getValorFil();
		  int columna = movimiento.getValorCol();
	      int j = columna - 1;
	      while (tab[j][fila] != jugador) {
	    	  tab[j][fila] = jugador;
	         j--;
	      }
	   }

	   
	   private void convertirFichasArribaDerecha(MovimientoReversi movimiento) {
		  int fila = movimiento.getValorFil();
		  int columna = movimiento.getValorCol();
	      int i = fila - 1;
	      int j = columna + 1;
	      while (tab[j][i] != jugador) {
	    	  tab[j][i] = jugador;
	         i--;
	         j++;
	      }
	   }

	   
	   private void convertirFichasArribaIzquierda(MovimientoReversi movimiento) {
		  int fila = movimiento.getValorFil();
		  int columna = movimiento.getValorCol();
	      int i = fila - 1;
	      int j = columna - 1;
	      while (tab[j][i] != jugador) {
	         tab[j][i] = jugador;
	         i--;
	         j--;
	      }
	   }

	   
	   private void convertirFichasAbajoDerecha(MovimientoReversi movimiento) {
		  int fila = movimiento.getValorFil();
		  int columna = movimiento.getValorCol();
	      int i = fila + 1;
	      int j = columna + 1;
	      while (tab[j][i] != jugador) {
	    	  tab[j][i] = jugador;
	         i++;
	         j++;
	      }
	   }

	   
	   private void convertirFichasAbajoIzquierda(MovimientoReversi movimiento) {
		  int fila = movimiento.getValorFil();
		  int columna = movimiento.getValorCol();
	      int i = fila + 1;
	      int j = columna - 1;
	      while (tab[j][i] != jugador) {
	    	  tab[j][i] = jugador;
	         i++;
	         j--;
	      }
	   }
	   
	   void situarFicha(MovimientoReversi movimiento) {
		      boolean[] movimientoCorrecto = comprobarMovimientoCorrecto(movimiento);
		      if (movimientoCorrecto[0]) {
		         convertirFichasArriba(movimiento);
		      }
		      if (movimientoCorrecto[1]) {
		         convertirFichasAbajo(movimiento);
		      }
		      if (movimientoCorrecto[2]) {
		         convertirFichasDerecha(movimiento);
		      }
		      if (movimientoCorrecto[3]) {
		         convertirFichasIzquierda(movimiento);
		      }
		      if (movimientoCorrecto[4]) {
		         convertirFichasArribaDerecha(movimiento);
		      }
		      if (movimientoCorrecto[5]) {
		         convertirFichasArribaIzquierda(movimiento);
		      }
		      if (movimientoCorrecto[6]) {
		         convertirFichasAbajoDerecha(movimiento);
		      }
		      if (movimientoCorrecto[7]) {
		         convertirFichasAbajoIzquierda(movimiento);
		      }
		   }
	
	@Override
	public String tableroToString() {
		String cadena = "";

		for (int i = 0; i < COLUMNAS; i++) {
			for (int j = 0; j < FILAS; j++) {
				cadena += tab[i][j];
			}
		}

		System.out.println(cadena);
		return cadena;
	}

	@Override
	public void stringToTablero(String cadena) throws ExcepcionJuego {
		
		for(int k=0; k<cadena.length(); k++){
			for(int i=0; i < COLUMNAS; i++){
				for (int j=0, cont=0; j < FILAS && cont<cadena.length(); j++){
					tab[i][j] = Integer.parseInt(cadena.substring(cont, cont+1));
					cont++;
				}
			}
		}
		
	}

	@Override
	public String toString() {		
		System.out.print("|   0 1 2 3 4 5 6 7 |\n");
		for (int j = 0; j < FILAS; j++) {
			System.out.print("| " + j + " ");
			for (int i = 0; i < COLUMNAS; i++) {
				System.out.print(tab[i][j] + " ");
			}
			System.out.print("|");
			System.out.print("\n");
		}
		return null;
	}

}
